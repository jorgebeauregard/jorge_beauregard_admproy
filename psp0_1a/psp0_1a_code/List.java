public class List{
	Node head;

	public boolean insertarElemento(Node newNode){
		if(head==null){
			head=newNode;
		}
		else{
			Node head = this.head;
			while(head.next!=null){
				head=head.next;
			}
			head.next=newNode;
		}
		return true;
	}

	public void imprimirLista(){
		if(head==null) return;
		Node head = new Node();
		head=this.head;
		while(head!=null){
			System.out.print(head.data + " -> ");
			head=head.next;
		}
		System.out.println();
	}
}