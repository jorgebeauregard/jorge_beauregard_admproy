class Data{
	double media;
	double desv_est;
	int nodos=0;
	List lista;

	Data(List lista){
		media=0;
		desv_est=0;
		this.lista = lista;
	}
	
	public double calcularMedia(){
		double acumulador=0;
		Node n = new Node();
		n = lista.head;
		if(n==null){
			return(-1);
		}
		while(n!=null){
			acumulador+=n.data;
			n=n.next;
			nodos++;
		}
		media=acumulador/nodos;
		acumulador=0;
		return(media);
	}

	public double calcularDesviacionEstandar(){
		double acumulador=0;
		nodos=0;
		if(media==-1){
			return(-1);
		}
		Node n = new Node();
		n = lista.head;
		while(n!=null){
			acumulador+=Math.pow((n.data-media),2);
			nodos++;
			n=n.next;
		}
		desv_est=acumulador/(nodos-1);
		desv_est=Math.sqrt(desv_est);
		return(desv_est);
	}
}