public class Node{
	double data;
	Node next;

	Node(double data){
		this.data=data;
		next = null;
	}

	Node(){
		data = 0;
		next=null;
	}
}