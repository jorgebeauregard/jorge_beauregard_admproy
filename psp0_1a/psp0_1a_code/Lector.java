import java.util.Scanner;

public class Lector{
	Scanner sc;

	Lector(){
		sc = new Scanner(System.in);
	}

	List leerDatos(){
		List miLista = new List();
		while(sc.hasNext()){
			double data = sc.nextDouble();
			Node newNode = new Node(data);
			miLista.insertarElemento(newNode);
		}
		sc=null;
		return miLista;
	}
}