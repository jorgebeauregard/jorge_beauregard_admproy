import java.util.*;
public class LinearRegression {

	public LinearRegression() {
	}

	public double[] linearRegression(ArrayList<Double> dataset_x, ArrayList<Double> dataset_y) {
		Mean meanCalc = new Mean();
		double n = (double)dataset_x.size();
		double mean_x = meanCalc.mean(dataset_x);
		double mean_y = meanCalc.mean(dataset_y);

		System.out.println("Medias bien");
		ArrayList<Double>null_list = new ArrayList<Double>();

		double sum_xy = summatory(dataset_x, dataset_y, 1);
		double sum_x = summatory(dataset_x, null_list, 1);
		double sum_y = summatory(null_list, dataset_y, 1);
		double sum_x_squared = summatory(dataset_x, null_list, 2);
		double sum_y_squared = summatory(null_list, dataset_y, 2);


		double[] ans = lr(n, mean_x, mean_y, sum_xy, sum_x, sum_y, sum_x_squared, sum_y_squared);
		return ans;
	}

	public double summatory(ArrayList<Double> x, ArrayList<Double> y, int power){
		double sum = 0;
		if(!x.isEmpty() && !y.isEmpty()){
			for(int i = 0; i < x.size(); i++){
				sum += Math.pow(x.get(i) * y.get(i), power);
			}
		}
		if(x.isEmpty() && !y.isEmpty()){
			for(int i = 0; i < y.size(); i++){
				sum += Math.pow(y.get(i),power);
			}
		}
		if(y.isEmpty() && !x.isEmpty()){
			for(int i = 0; i < x.size(); i++){
				sum += Math.pow(x.get(i), power);
			}
		}

		return sum;
	}

	public double[] lr(double n, double mean_x, double mean_y, double sum_xy, double sum_x, double sum_y, double sum_x_squared, double sum_y_squared){
		double[] results = new double[4];
		
		double b1 = (sum_xy-(n*mean_x*mean_y))/(sum_x_squared-(n*Math.pow(mean_x,2)));
		
		double b0 = mean_y-(b1*mean_x);
	
		double r_xy = ((n * sum_xy) - (sum_x*sum_y)) / (Math.sqrt( (n*sum_x_squared-Math.pow(sum_x,2)) * (n*sum_y_squared-Math.pow(sum_y,2)) ));

		double r_squared = Math.pow(r_xy,2);

		results[0] = b1;
		results[1] = b0;
		results[2] = r_xy;
		results[3] = r_squared;

		return results;
	}

}
