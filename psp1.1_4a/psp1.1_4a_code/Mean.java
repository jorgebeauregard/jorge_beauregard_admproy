import java.util.*;
public class Mean {
	public Mean() {
	}

	public double mean(ArrayList<Double> dataset) {
		double mean = 0;
		double sum = 0;
		for(int i = 0; i < dataset.size(); i++){
			sum += dataset.get(i);
		}

		mean = sum / dataset.size();

		return mean;
	}

}
