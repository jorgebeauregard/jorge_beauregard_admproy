import java.io.IOException;
import java.util.*;

public class FileRead{
    public static String path, pathNew;
    public static ReadFile file,fileNew;
    public static String[] fileLines, fileLinesNew;
    public static int GENERALCOUNT = 0;
    public static int GENERALCOUNTNEW = 0;
    public static boolean comment = false;
    public static List<String> Objects = new ArrayList<String>();
    public static List<Integer> ObjectsStart = new ArrayList<Integer>();
    public static List<Integer> ObjectsEnd = new ArrayList<Integer>();
    public static List<Integer> ObjectsMethods = new ArrayList<Integer>();
    public static List<Character> BracesOpen = new ArrayList<Character>();
    public static List<Character> BracesClosed = new ArrayList<Character>();

    public static int[] methodLines() throws IOException{
      try{
        Scanner input = new Scanner(System.in);
        path = "";
        System.out.println("Please enter a program to input: ");
        String newString = input.next();
        while(newString.isEmpty()){
          System.out.println("Please enter a not null input");
          newString = input.next();
        }
          path += newString;
        file = new ReadFile(path);
        fileLines = file.OpenFile();
        int n = countLOC(0, fileLines.length);
        objectCounter();
        for(int i = 0; i < Objects.size(); i++){
          objectEnd(ObjectsStart.get(i));
        }
        int[] res = new int[2];
        System.out.printf("PROGRAM #\tObjectName\tNo. of Methods\tObject LOC\t Total program LOC\n");
        for(int i = 0; i < Objects.size(); i++){
          int myMethods = ObjectsMethods.get(i);
          int linesofMethods = countLOC(ObjectsStart.get(i), ObjectsEnd.get(i));
          System.out.printf("0 \t\t %s \t\t %d \t\t %d \t\t-\n",Objects.get(i),(int)myMethods,linesofMethods);
          res[0] = myMethods;
          res[1] = linesofMethods;
        }
        System.out.printf("0\t\t\t\t\t\t\t\t\t%d\n", countLOC(0,ObjectsEnd.get(0)));


        return res;

      }
      catch(Exception e){
        System.out.println("An error occured. Please rerun de program.");
        return null;
      }
    }



    public static int countLOC(int start, int end){
        int count = 1;
        for(int i = start; i < end; i++){
            fileLines[i] = fileLines[i].replaceAll("\t",""); 
            fileLines[i] = fileLines[i].replaceAll(" ","");
            if(!fileLines[i].isEmpty() && fileLines[i].length() > 1){ 
              if(fileLines[i].charAt(0) == '/' &&
                      (fileLines[i].charAt(1) == '/' || fileLines[i].charAt(1) == '*') && !comment){
                if(fileLines[i].charAt(0) == '/' && fileLines[i].charAt(1) == '*'){
                  comment = true;
                }
              }
              else if(fileLines[i].contains("*/") && comment){
                comment = false;
              }
              else if(!comment){
                count++;
              }
            }
            else if(!fileLines[i].isEmpty() && fileLines[i].equals("}")){
              count++;
            }
        }
        comment = false;
        return count;
    }

 
    public static void objectCounter() throws IOException{
      Objects.clear();
      ObjectsStart.clear();
      ObjectsEnd.clear();
      BracesOpen.clear();
      BracesClosed.clear();
      fileLines = file.OpenFile();



      for(int i = 0; i < fileLines.length; i++){
        if(!fileLines[i].isEmpty()){
          if(fileLines[i].contains("public") || fileLines[i].contains("private") || fileLines[i].contains("protected")){
            if(fileLines[i].contains("class")){
              fileLines[i] = fileLines[i].replaceAll("\t","");
              String[] lineAr = fileLines[i].split(" ");
              int j = 0;
              while(j < lineAr.length){
                if(!lineAr[j].equals("public") && !lineAr[j].equals("private") &&
                  !lineAr[j].equals("public") && !lineAr[j].equals("class") &&
                  !lineAr[j].equals("static") && !lineAr[j].isEmpty()
                ){
                  Objects.add(lineAr[j]);
                  ObjectsStart.add(i);
                  break;
                }
                  j++;
              }
            }
          }
        }
      }
    }


    public static void objectEnd(int start) throws IOException{
      fileLines = file.OpenFile();
      BracesOpen.clear();
      BracesClosed.clear();
      BracesOpen.add('{');
      int end = start;
      for(int i = start+1; i < fileLines.length+1; i++){
        if(BracesOpen.size() != BracesClosed.size() && i<fileLines.length){
          if(!comment){
            if(fileLines[i].contains("{")){
              BracesOpen.add('{');
            }
            if(fileLines[i].contains("}")){
              BracesClosed.add('}');
            }
          }
        }
        else{
          end = i-1;
          break;
        }
      }
      ObjectsEnd.add(end);

      int noOfMethods = 0;
      fileLines = file.OpenFile();
      for(int i = start+1; i < end; i++){
        if(!comment){
          if(!fileLines[i].isEmpty()){
            if(fileLines[i].contains("public") || fileLines[i].contains("private") || fileLines[i].contains("protected")){
              if(!fileLines[i].contains("class")){
                if(fileLines[i].contains("{")){
                  noOfMethods++;
                }
              }
            }
          }
        }
      }
      ObjectsMethods.add(noOfMethods);
    }

}
