public class Main{
    static Reader reader;
    static List data;
    static PredictionInterval pr;
    public static void main(String []args){
        reader = new Reader();
        data = reader.readData();
        pr = new PredictionInterval(data);
        pr.printAllData();
    }
}