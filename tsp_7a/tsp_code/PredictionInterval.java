class PredictionInterval{
    List xValues;
    List yValues;
    double xSubK;
    Function tFunction;
    LinearRegression lr;
    Significance sig;
    ValueP pCalculator;
    double error;
    double segments;

    double range;
    double upi;
    double lpi;

    PredictionInterval(List data){
        xSubK = data.xSubK;
        //Split values
        //region
        List x = new List();
        List y = new List();
        Node helper = data.head;
        int totalLength = data.length;
        for(int i = 0; i < totalLength/2; i++){
            x.insert(helper);
            helper = helper.next;
        }
        x.tail.next = null; //because the tail might be pointing to the head of y, we crop it. Yet we remember because helper points to it
        for(int i = 0; i < totalLength/2; i++){
            y.insert(helper);
            helper = helper.next;
        }
        y.tail.next = null;
        //endregion
        xValues = x;
        yValues = y;

        error = 0.00001;
        segments = 10;

        lr = new LinearRegression(xValues,yValues,xSubK);
        tFunction = new TFunction(lr.dataSetSize - 2);
        sig = new Significance(lr,tFunction,error,segments);
        pCalculator = new ValueP(tFunction,0.35,error,segments); //p and dof explicitly defined
        range = getRange();
        upi = getUpi();
        lpi = getLpi();
    }

    double getUpi(){
        double result = lr.ySubK + range;
        return result;
    }

    double getLpi(){
        double result = lr.ySubK - range;
        return result;
    }

    double getRange(){
        double specialStDev = getSpecialStDev();
        double rootPart = getRootPart();
        double notRootPart = pCalculator.x;
        double result = notRootPart*rootPart*specialStDev;
        return result;
    }

    double getSpecialStDev(){
        double leftPart = (1.0 / (lr.dataSetSize-2.0));
        double rightPart = 0;
        for(int i = 0; i < lr.dataSetSize; i++){
            Node xVal = xValues.head;
            Node yVal = yValues.head;
            rightPart+=Math.pow((yVal.data - lr.betaZero - (lr.betaOne*xVal.data)),2);
            xVal = xVal.next;
            yVal = yVal.next;
        }
        double result = Math.sqrt((leftPart*rightPart));

        return result;
    }

    double getRootPart(){
        double upperPart = Math.pow( (lr.xSubK - lr.statsXVal.mean) ,2);
        double lowerPart = 0;
        for(int i = 0; i < lr.dataSetSize; i++){
            Node xVal = xValues.head;
            lowerPart+=Math.pow( (xVal.data - lr.statsXVal.mean) ,2);
            xVal = xVal.next;
        }

        double result = Math.sqrt((1 + (1/lr.dataSetSize) + (upperPart/lowerPart)));

        return result;
    }

    void printDataPairs(){
        Node xVal = this.xValues.head;
        Node yVal = this.yValues.head;
        System.out.printf("Data pairs:\n");
        while(xVal != null && yVal != null){
            System.out.printf("%.02f , %.02f\n",xVal.data,yVal.data);
            xVal = xVal.next;
            yVal = yVal.next;
        }
    }

    void printAllData(){
        printDataPairs();
        System.out.println("rSubXY:    " + lr.rSubXY);
        System.out.println("rSquared:  " + lr.rSquared);
        System.out.println("Tail Area: " + sig.tailArea);
        System.out.println("betaZero:  " + lr.betaZero);
        System.out.println("betaOne:   " + lr.betaOne);
        System.out.println("ySubK:     " + lr.ySubK);
        System.out.println("Range:     " + this.range);
        System.out.println("UPI:       " + this.upi);
        System.out.println("LPI:       " + this.lpi);
    }



}