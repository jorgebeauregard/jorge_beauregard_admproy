import java.util.Scanner;
public class Reader{
    Scanner sc;    
    List readData(){
        sc = new Scanner(System.in);
        double xSubK = sc.nextDouble();
        List nList = new List(xSubK);
        while(sc.hasNext()){
            double data = sc.nextDouble();
            Node newNode = new Node(data);
            nList.insert(newNode);
        }
        sc.close();
        sc = null;
        return nList;
    }
}