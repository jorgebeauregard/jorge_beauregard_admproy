public class List{
    Node head;
    Node tail;
    int length;
    double xSubK;
    List(double xSubK){
        head = null;
        tail = null;
        length = 0;
        this.xSubK = xSubK;
    }

    List(){
        head = null;
        tail = null;
        length = 0;
        this.xSubK = 0;
    }
    void insert(Node newNode){
        try{
            length++;
        }
        catch(Exception e){
            System.out.println("Length counter overflown");
            System.out.println("Wont be able to keep track of length, but insertions will be allowed");
        }
        if(head == null) {
            head = newNode;
            tail = newNode;
        }
        else{
            tail.next = newNode;
            tail = newNode;
        } 
    }
    void printList(){
        if(this.head == null) return;
        Node head = this.head;
        while(head != null){
            System.out.printf("%.2f ",head.data);
            head = head.next;
        }
        System.out.println();
    }
}