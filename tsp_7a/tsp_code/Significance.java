class Significance{
    LinearRegression lr;
    Simpson integrate;
    Function tFunction;

    double tailArea; //Actual result of significance
    double upperLimit; //Upper limit of integration, x
    double probabilityValue; //p value

    Significance(LinearRegression lr, Function tFunction, double error, double segments){
        this.lr = lr;
        this.tFunction = tFunction;
        upperLimit = obtainUpperLimit();
        integrate = new Simpson(this.tFunction,10,0.00001,upperLimit); //gets probability
        probabilityValue = integrate.p; //Explicitly indicate that probabilityValue is obtained
        tailArea = obtainTailArea();
    }


    double obtainUpperLimit(){
        double upperThing = Math.abs(lr.rSubXY) * Math.sqrt(lr.dataSetSize-2);
        double lowerThing = Math.sqrt(1 - Math.pow(lr.rSubXY,2));

        double result = upperThing/lowerThing;

        return result;
    }

    double obtainTailArea(){
        double result = 1 - (2*this.probabilityValue);

        return result;
    }
}