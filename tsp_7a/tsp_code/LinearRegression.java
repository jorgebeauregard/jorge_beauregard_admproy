public class LinearRegression{
    List xValues;
    List yValues;
    StatisticalData statsXVal;
    StatisticalData statsYVal;
    int dataSetSize;
    double betaOne;
    double betaZero;
    double rSubXY;
    double rSquared;
    double xSubK;
    double ySubK;

    //Constructor with one list
    LinearRegression(List data){
        //Split values
        List x = new List();
        List y = new List();
        Node helper = data.head;
        int totalLength = data.length;
        for(int i = 0; i < totalLength/2; i++){
            x.insert(helper);
            helper = helper.next;
        }
        x.tail.next = null; //because the tail might be pointing to the head of y, we crop it. Yet we remember because helper points to it
        for(int i = 0; i < totalLength/2; i++){
            y.insert(helper);
            helper = helper.next;
        }
        y.tail.next = null;

        this.xValues = x;
        this.yValues = y;
        this.dataSetSize = x.length;
        this.statsXVal = new StatisticalData(this.xValues);
        this.statsYVal = new StatisticalData(this.yValues);
        this.betaOne = calculateBetaOne();
        this.betaZero = calculateBetaZero();
        this.rSubXY = calculateRSubXY();
        this.rSquared = calculateRSquared();
        this.xSubK = data.xSubK;
        this.ySubK = calculateYSubK();
    }

    //Constructor with two lists
    LinearRegression(List x, List y, double xSubK){
        this.xValues = x;
        this.yValues = y;
        this.dataSetSize = x.length;
        this.statsXVal = new StatisticalData(this.xValues);
        this.statsYVal = new StatisticalData(this.yValues);
        this.betaOne = calculateBetaOne();
        this.betaZero = calculateBetaZero();
        this.rSubXY = calculateRSubXY();
        this.rSquared = calculateRSquared();
        this.xSubK = xSubK;
        this.ySubK = calculateYSubK();
    }

    double calculateSumOfProductsOfXTimesY(){
        double sum = 0;
        try{
            Node xNode = this.xValues.head;
            Node yNode = this.yValues.head;
            while(xNode != null && yNode != null){
                sum+=(xNode.data*yNode.data);
                xNode = xNode.next;
                yNode = yNode.next;
            }
        }
        catch(Exception e){
            System.out.println("List might not have been initialized");
            return -1;
        }
        return sum;
    }
    double calculateSumOfSquaredXValues(){
        double sum = 0;
        try{
            Node xNode = this.xValues.head;
            while(xNode != null){
                sum+=Math.pow(xNode.data,2);
                xNode = xNode.next;
            }
        }
        catch(Exception e){
            System.out.println("List might not have been initialized");
            return -1;
        }
        return sum;
    }
    double calculateSumOfSquaredYValues(){
        double sum = 0;
        try{
            Node yNode = this.yValues.head;
            while(yNode != null){
                sum+=Math.pow(yNode.data,2);
                yNode = yNode.next;
            }
        }
        catch(Exception e){
            System.out.println("List might not have been initialized");
            return -1;
        }
        return sum;
    }
    double calculateSumOfXValues(){
        double sum = 0;
        try{
            Node xNode = this.xValues.head;
            while(xNode != null){
                sum+=xNode.data;
                xNode = xNode.next;
            }
        }
        catch(Exception e){
            System.out.println("List might not have been initialized");
            return -1;
        }
        return sum;
    }
    double calculateSumOfYValues(){
        double sum = 0;
        try{
            Node yNode = this.yValues.head;
            while(yNode != null){
                sum+=yNode.data;
                yNode = yNode.next;
            }
        }
        catch(Exception e){
            System.out.println("List might not have been initialized");
            return -1;
        }
        return sum;
    }
    double calculateBetaOne(){
        try{
            double sumOfProductsOfXandY = this.calculateSumOfProductsOfXTimesY();
            double dataSetSizeTimesAvgXSetTimesAvgYSet = this.dataSetSize * this.statsXVal.mean * this.statsYVal.mean;
            double sumOfSquaredXValues = this.calculateSumOfSquaredXValues();
            double dataSetSizeTimesSquaredXAvg = this.dataSetSize * Math.pow(this.statsXVal.mean,2);

            double betaOne = (sumOfProductsOfXandY - dataSetSizeTimesAvgXSetTimesAvgYSet) / (sumOfSquaredXValues - dataSetSizeTimesSquaredXAvg);
            return betaOne;
        }
        catch(Exception e){
            System.out.println("Lists might have not been initialized");
            return -1;
        }
    }
    double calculateBetaZero(){
        try{
            if(this.betaOne == 0) this.betaOne = calculateBetaOne();

            double betaZero = this.statsYVal.mean - (this.betaOne * this.statsXVal.mean);
            return betaZero;
        }
        catch(Exception e){
            System.out.println("Lists might have not been initialized");
            return -1;
        }
    }
    double calculateRSubXY(){
        try{
            double sumOfProductsOfXandYTimesdatSetSize = this.dataSetSize * this.calculateSumOfProductsOfXTimesY();
            double productOfSumOfXValuesAndSumOfYValues = this.calculateSumOfXValues() * this.calculateSumOfYValues();
            double sumOfSquaredXValuesTimesDataSet = this.dataSetSize * this.calculateSumOfSquaredXValues();
            double sumOfXValues = this.calculateSumOfXValues();
            double sumOfSquaredYValuesTimesDataSet = this.dataSetSize * this.calculateSumOfSquaredYValues();
            double sumOfYValues = this.calculateSumOfYValues();

            double up = (sumOfProductsOfXandYTimesdatSetSize - productOfSumOfXValuesAndSumOfYValues);
            double down = (Math.sqrt((sumOfSquaredXValuesTimesDataSet - Math.pow(sumOfXValues,2))*(sumOfSquaredYValuesTimesDataSet - Math.pow(sumOfYValues,2))));

            double rSubXY = up/down;
        
            return rSubXY;
        }
        catch(Exception e){
            System.out.println("List might have not been initialzed");
            return -1;
        }
    }
    double calculateRSquared(){
        double rSquared = this.rSubXY*this.rSubXY;
        return rSquared;
    }
    double calculateYSubK(){
        double ySubK = this.betaZero + (this.betaOne*this.xSubK);
        return ySubK;
    }
}