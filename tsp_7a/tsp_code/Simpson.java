public class Simpson{
    Function func;
    double num_seg;
    double x;
    double w;
    double error;
    double p;
    
    //upper limit should be defined from withing the constructor.
    //may add a lower limit if needed. Here the lower limit is zero
    Simpson(Function func, double num_seg, double error, double upperLimit){
        this.func = func;
        this.num_seg = num_seg;
        this.x = upperLimit;
        this.w = this.x/this.num_seg;
        this.error = error;
        this.p = getP();

    }

    double getP(){
        double res1 = 1;
        double res2 = this.pFunction(this.num_seg);
        double diff = Math.abs(res2-res1);
        while(diff > this.error){
            res1 = res2;
            this.num_seg = this.num_seg*2;
            this.w = this.x/num_seg;
            res2 = this.pFunction(this.num_seg);
            diff = Math.abs(res2-res1); 
        }
        return res2;
    }
    double pFunction(double num_seg){
        double sum1 = 0;
        double sum2 = 0;
        for(int i = 1; i <= num_seg-1; i+=2){
            sum1+= 4*this.func.function(i*this.w);
        }
        for(int i = 2; i <= num_seg-2; i+=2){
            sum2+= 2*this.func.function(i*this.w);
        }

        double res = this.func.function(0) + sum1 + sum2 + this.func.function(this.x);

        return res*(this.w/3.0);
    }

}