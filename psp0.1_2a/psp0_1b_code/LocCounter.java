import java.util.*;

//The class LocCounter as its name says,
// counts valid program lines of an OOP code.
//#C
class LocCounter{
	//#F
	public static void main(String[] args){
		//#V
		Scanner sc = new Scanner(System.in);
		//Current code line
		String code = "";
		//stack for storing keys
		Stack<String> stk = new Stack<String>();
		//Stack for number of lines in section
		Stack<Integer> block = new Stack<Integer>();
		//stack for section names
		Stack<String> names = new Stack<String>();

		//Counters for in line.
		int current_counter = 0;
		int total_counter = 0;
		int in_class = 0;
		int in_var = 0;
		int in_function = 0;

		//Global counters
		int num_class = 0;
		int num_var = 0;
		int num_func = 0;
		int comments_directives = 0;
		//#N
		//ADDED ON 4/3/2017
		int num_new_code = 0;
		int num_deleted_code = 0;
		//##

		//to check if the next line is a name.
		boolean named_function = false;
		//##

		//Iterate trught all the code
		while(sc.hasNext()){
			//get the code line and clean it.
			code = sc.nextLine();
			code = code.replaceAll("	","");
			if(counterFunction(code)){
				//check if its a function
				stk.push("F");
				total_counter++;
				current_counter++;
				num_func++;
				block.push(current_counter);
				current_counter = 0;
				named_function = true;
			}else if(counterVar(code)){
				//check if its a Var block
				stk.push("V");
				total_counter++;
				current_counter++;
				num_var++;
				block.push(current_counter);
				current_counter = 0;
				named_function = true;
			}else if(counterClass(code)){
				//check if its a class
				stk.push("C");
				total_counter++;
				current_counter++;
				num_class++;
				block.push(current_counter);
				current_counter = 0;
				named_function = true;
			}else if(counterNewBlock(code)){
				//Check for new code
				//#N
				//ADDED ON 4/3/2017
				stk.push("N");
				total_counter++;
				current_counter++;
				num_new_code++;
				block.push(current_counter);
				current_counter = 0;
				named_function = true;
				//##
			}else if(counterDeletedLines(code)){
				//Check for deleted lines
				//#N
				//ADDED ON 4/3/2017
				total_counter++;
				current_counter++;
				String num = code.replaceAll("\\D+","");
				try{
				int lines = Integer.parseInt(num);
				num_deleted_code += lines;	
				}catch(Exception e)
				{
					num_deleted_code++;
				}
				//##
			}else if(counterClose(code)){
				//check if section finishes
				total_counter++;
				current_counter++;
				//Check section ID
				switch(stk.peek())
				{
					case "F":in_function+=current_counter;break;
					case "V":in_var+=current_counter;break;
					case "C":in_class+=current_counter;break;
					case "N":num_new_code+=current_counter;break;
				}
				System.out.printf("In '%s' : %d lines \n", names.pop(), current_counter);
				current_counter += block.pop();
				stk.pop();
			}else{
				//Check for valid line
				if(code.length()>1)
				{
					total_counter++;
					current_counter++;
					if(named_function)
					{
						//Obtain fuction, class or section name
						named_function = false;
						names.push(code);
					}
				}
			}
			//Check for comment section
			if(code.contains("//") && !code.contains("\"//")){
				comments_directives++;
			}
		}
		//Print final status of counters
		System.out.printf("Classes: %d\nVar Blocks: %d\nFuctions in class: %d\n",num_class,num_var,num_func);
		System.out.printf("Comments and Directives: %d\n",comments_directives);
		System.out.printf("Total LOC: %d \n",total_counter);
		System.out.printf("New lines: %d \n",num_new_code);
		System.out.printf("Deleted Lines: %d \n", num_deleted_code);
	}
	//##

	
	//#F
	private static boolean counterFunction(String s){
		//Check if line is a function
		//Check also that the line is not checking for the same
		if(s.contains("//#F") && !s.contains("\"//#F\""))
		{
			return true;
		}
		return false;
	}
	//##

	//#F
	private static boolean counterVar(String s){
		//Check if line is a Var Block
		//Check also that the line is not checking for the same
		if(s.contains("//#V") && !s.contains("\"//#V\""))
		{
			return true;
		}
		return false;
	}
	//##

	//#F
	private static boolean counterClass(String s){
		//Check if line is a Class
		//Check also that the line is not checking for the same
		if(s.contains("//#C") && !s.contains("\"//#C\"")){
			return true;
		}
		return false;
	}
	//##

	//#F
	private static boolean counterClose(String s)
	{
		//Check if directive closes
		//Check also that the line is not checking for the same
		if(s.contains("//##") && !s.contains("\"//##\"")){
			return true;
		}
		return false;
	}
	//##

	//#N
	//ADDED ON 4/3/2017
	//#F
	private static boolean counterNewBlock(String s){
		//Check if new code block was added
		//Check also that the line is not checking for the same
		if(s.contains("//#N") && !s.contains("\"//#N\"")){
			return true;
		}
		return false;
	}
	//##
	//##

	//#N
	//ADDED ON 4/3/2017
	//#F
	private static boolean counterDeletedLines(String s){
		//Check if new code block was added
		//Check also that the line is not checking for the same
		if(s.contains("//#D") && !s.contains("\"//#D\"")){
			return true;
		}
		return false;
	}
	//##
	//##
}
//##