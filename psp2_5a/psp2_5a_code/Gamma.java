import java.util.*;

public class Gamma{
  public double gammaFunction(double x){
    if(x == 1){
      return 1;
    }
    else if(x == 0.5){
      return Math.sqrt(3.1416);
    }
    else{
      return (x-1)*gammaFunction(x-1);
    }
  }
}
