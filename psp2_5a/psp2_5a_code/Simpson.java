import java.util.*;

public class Simpson{
  double p, num_seg, w, error, dof, x;
  ArrayList<Double> results;

  public Simpson(double num_seg, double error, double dof, double x){
    this.num_seg = num_seg;
    this.x = x;
    this.error = error;
    this.dof = dof;
    this.w = this.x/this.num_seg;
    this.results = new ArrayList<Double>();
    this.p = 0;
  }

  public double simpsons(){
    double simpsAct = ((this.w/3)*F(0)) + summatory(4) + summatory(2) + ((this.w/3)*F(this.x));
    double simpsAnt = 0;
    while(Math.abs(simpsAct-simpsAnt) > error){
      simpsAnt = simpsAct;
      this.num_seg *= 2;
      this.w = this.x/this.num_seg;
      double sum1 = summatory(4);
      double sum2 = summatory(2);
      simpsAct = ((this.w/3)*F(0)) + summatory(4) + summatory(2) + ((this.w/3)*F(this.x));
    }
    return simpsAct;
  }

  public double summatory(double multiplier){
    double res = 0;
    if(multiplier == 4){
      for(int i = 1; i <= num_seg - 1; i= i+2){
        double resParcial = ((this.w/3)*(multiplier * F(i*this.w)));
        res+= resParcial;
      }
    }
    if(multiplier == 2){
      for(int i = 2; i <= num_seg - 2; i= i+ 2){
        double resParcial = ((this.w/3)*(multiplier * F(i*this.w)));
        res+= resParcial;
      }
    }
    return res;
  }

  public double F(double x){
    Gamma gf = new Gamma();
    double res1 = 0;
    double res2 = 0;
    double res = 0;
    res1 = gf.gammaFunction((dof+1)/2)/(Math.pow((dof*3.1416),0.5)*gf.gammaFunction(dof/2));
    res2 = Math.pow((1+((x*x)/dof)), -(dof+1)/2);
    return res1*res2;
  }

}
