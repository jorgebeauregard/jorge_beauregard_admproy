import java.util.Scanner;

public class Main{
  public static void main(String[] args){
    Scanner input = new Scanner(System.in);

    System.out.println("Enter num_seg:");
    double num_seg = input.nextDouble();
    System.out.println("Enter x:");
    double x = input.nextDouble();
    System.out.println("Enter E:");
    double error = input.nextDouble();
    System.out.println("Enter dof:");
    double dof = input.nextDouble();
    
    Simpson simpsonsCalc = new Simpson(num_seg, error, dof, x);
    Gamma gammaFunctionCalc = new Gamma();

    System.out.println(simpsonsCalc.simpsons());

  }
}