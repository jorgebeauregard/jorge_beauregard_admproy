import java.lang.Math;

public class TFunction extends Function{
    double dof;

    TFunction(double dof, double x){
        super(x);
        this.dof = dof;
    }

    double function(double x){
        double pow = -1 * ( (dof+1)/2.0 );
        double notPow = 1 + (Math.pow(x,2)/dof);
        double up = gammaFunction(((dof+1)/2.0))*(Math.pow(notPow,pow));
        double down = Math.sqrt((dof*Math.PI)) * gammaFunction((dof/2.0));

        return up/down;
    }

    double gammaFunction(double x){
        if(x == 1.0) 
            return 1.0;
        else if(x == 0.5) 
            return Math.sqrt(Math.PI);
        else 
            return (x-1)*gammaFunction(x-1);
    }
}