import java.util.Scanner;

public class Main{
    public static void main(String []args){
        Scanner sc = new Scanner(System.in);
        double num_seg = sc.nextDouble();
        double error = sc.nextDouble();
        double dof = sc.nextDouble();
        
        double p = sc.nextDouble();
        Function theFunc = new TFunction(dof,1);
        ValueP myValueP = new ValueP(theFunc,p,error,num_seg);

        System.out.println(myValueP.x);
    }
}