public class StatisticalData{
    double mean;
    double sumOfValueMinusMeanSquared;
    double varianza; 
    double desvest; 
    
    List data;

    StatisticalData(List data){
        this.data = data;
        mean = calculateMean();
        sumOfValueMinusMeanSquared = calculateSumOfValueMinusMeanSquared();
        varianza = calculateVarianza();
        desvest = calculateDesvEst();
    }

    double calculateMean(){
        double sum = 0;
        Node node = this.data.head;
        while(node != null){
            sum+=node.data;
            node = node.next;
        }
        double mean = sum/this.data.length;
        return mean;
    }

    double calculateSumOfValueMinusMeanSquared(){
        if(this.mean == 0) return 0;
        Node node = this.data.head;
        double bigSum = 0;
        while(node != null){
            bigSum+=Math.pow( (node.data - mean),2 );
            node = node.next;
        }

        return bigSum;
    }

    double calculateVarianza(){
        if(this.mean == 0) return 0;
        double bigSum = this.calculateSumOfValueMinusMeanSquared();
        double varianza = bigSum/(this.data.length - 1);
        return varianza;
    }

    double calculateDesvEst(){
        if(this.mean == 0) return 0;
        if(this.varianza == 0) this.varianza = calculateVarianza();
        double desvest = Math.sqrt(varianza);
        return desvest;
    }

    void printData(){
        System.out.printf("%s","Data set: ");
        this.data.printList();
        System.out.printf("Mean: %.02f\n",this.mean);
        System.out.printf("Standard Deviation: %.02f\n",this.desvest);
    }
}