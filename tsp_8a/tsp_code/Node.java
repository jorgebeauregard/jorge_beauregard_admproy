public class Node{
    double data;
    Node next;
    Node(double data){
        next = null;
        this.data = data;
    }   
}