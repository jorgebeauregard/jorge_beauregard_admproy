class ValueP{
    Simpson integrate;
    double p;
    double x;
    double error;

    ValueP(Function func, double p, double error, double segments){
        this.p = p;
        this.error = error;
        this.x = getX(1,func, segments);
    }

    //x to be returned is in func.x
    double getX(double xTest,Function func, double segments){
        integrate = new Simpson(func,segments,this.error,xTest);
        double diff;
        diff = integrate.p - this.p;
        if(diff < 0) diff*=-1;
        while(diff > this.error){
            if(integrate.p < this.p)
                xTest+=(xTest/2);
            if(integrate.p > this.p)
                xTest-=(xTest/2);
            integrate = new Simpson(func,segments,this.error,xTest);
            diff = integrate.p - this.p;
            if(diff < 0) diff*=-1;
        }
        return xTest;
    }
}