class MultipleRegression{
    //Wk = 650. Xk = 3000. Yk = 155. Zk = ? 
    //z = betaZero + betaOne + betaTwo + betaThree
    double betaZero;  //linear regression results
    double betaOne;   //linear regression results
    double betaTwo;   //linear regression results
    double betaThree; //linear regression results

    double dataLength;

    double wSubK;
    double xSubK;
    double ySubK;
    double zSubK; //actual result

    List wValues;
    List xValues;
    List yValues;
    List zValues;

    StatisticalData wStats;
    StatisticalData xStats;
    StatisticalData yStats;
    StatisticalData zStats;

    Gauss gaussSolver;

    ValueP pValCalc;

    Function tFunc;

    double range;
    double upi;
    double lpi;

    MultipleRegression(List data){
        this.dataLength = data.length/4;

        List w = new List();
        List x = new List();
        List y = new List();
        List z = new List();

        Node helper = data.head;
        for(int i = 0; i < this.dataLength; i++){
            w.insert(helper);
            helper = helper.next;
        }
        w.tail.next = null;

        for(int i = 0; i < this.dataLength; i++){
            x.insert(helper);
            helper = helper.next;
        }
        x.tail.next = null;

        for(int i = 0; i < this.dataLength; i++){
            y.insert(helper);
            helper = helper.next;
        }
        y.tail.next = null;

        for(int i = 0; i < this.dataLength; i++){
            z.insert(helper);
            helper = helper.next;
        }
        z.tail.next = null;

        this.wValues = w;
        this.xValues = x;
        this.yValues = y;
        this.zValues = z;

        wStats = new StatisticalData(this.wValues); 
        xStats = new StatisticalData(this.xValues); 
        yStats = new StatisticalData(this.yValues); 
        zStats = new StatisticalData(this.zValues); 

        wSubK = data.wSubK;
        xSubK = data.xSubK;
        ySubK = data.ySubK;
        zSubK = calculateZSubK();

        tFunc = new TFunction(this.dataLength-4);
        pValCalc = new ValueP(tFunc,0.35,0.00001,10);

        range  = calculateRange();
        upi = zSubK + range;
        lpi = zSubK - range;
    }

    double calculateRange(){
        double someXValue = pValCalc.x;
        double stdev = calculateStdev();
        double bigSqrtParty = calculateBigSqrtParty();

        double result = someXValue*stdev*bigSqrtParty;
        return result;
    }

    double calculateBigSqrtParty(){
        Node helper;

        double easyPart = 1.0 + (1.0/this.dataLength);

        double firstUpperPart = Math.pow((this.wSubK - this.wStats.mean),2);
        double firstLowerPart = 0;
        helper = wValues.head;
        for(int i = 0; i < this.dataLength; i++){
            firstLowerPart += Math.pow((helper.data - this.wStats.mean),2);
            helper = helper.next;
        }

        double secondUpperPart = Math.pow((this.xSubK - this.xStats.mean),2);
        double secondLowerPart = 0;
        helper = xValues.head;
        for(int i = 0; i < this.dataLength; i++){
            secondLowerPart += Math.pow((helper.data - this.xStats.mean),2);
            helper = helper.next;
        }

        double thirdUpperPart = Math.pow((this.ySubK - this.yStats.mean),2);
        double thirdLowerPart = 0;
        helper = yValues.head;
        for(int i = 0; i < this.dataLength; i++){
            thirdLowerPart += Math.pow((helper.data - this.yStats.mean),2);
            helper = helper.next;
        }

        double insideRoot = easyPart + (firstUpperPart/firstLowerPart) + (secondUpperPart/secondLowerPart) + (thirdUpperPart/thirdLowerPart);

        double result = Math.sqrt(insideRoot);

        return result;

    }

    double calculateStdev(){
        double leftPart = (1.0/(this.dataLength-4.0));
        double rightPart = 0;

        Node wVal = this.wValues.head;
        Node xVal = this.xValues.head;
        Node yVal = this.yValues.head;
        Node zVal = this.zValues.head;

        for(int i = 0; i < this.dataLength; i++){
            rightPart+=Math.pow((zVal.data - this.betaZero - (this.betaOne*wVal.data) - (this.betaTwo*xVal.data) - (this.betaThree*yVal.data)),2);
            
            wVal = wVal.next;
            xVal = xVal.next;
            yVal = yVal.next;
            zVal = zVal.next;
        }

        double result = Math.sqrt(leftPart*rightPart);
        return result;
    }

    double calculateZSubK(){
        double [][] mainMatrix = buildMainMatrix();
        double [] resultArr = buildResultArr();
        gaussSolver = new Gauss(mainMatrix,resultArr);
        double [] betaValues = gaussSolver.coefficients;

        this.betaZero  = betaValues[0];
        this.betaOne   = betaValues[1];
        this.betaTwo   = betaValues[2];
        this.betaThree = betaValues[3];

        double result = this.betaZero + (this.wSubK*this.betaOne) + (this.xSubK*this.betaTwo) + (this.ySubK*this.betaThree);

        return result;
    }

    double[][] buildMainMatrix(){

        double[] firstRow  = buildFirstRow();
        double[] secondRow = buildSecondRow();
        double[] thirdRow  = buildThirdRow();
        double[] fourthRow = buildFourthRow();

        double[][] result = {firstRow,secondRow,thirdRow,fourthRow};

        return result;
        //builds the main matrix
    }

    double[] buildFirstRow(){
        double betaZeroTimesN;       //n
        double betaOneTimesSumOfW;   //sum of w
        double betaTwoTimesSumOfX;   //sum of x
        double betaThreeTimesSumOfY; //sum of y

        Node helper;

        betaZeroTimesN = this.dataLength;

        betaOneTimesSumOfW = 0;
        helper = wValues.head;
        for(int i = 0; i < this.dataLength; i++){
            betaOneTimesSumOfW+=helper.data;
            helper = helper.next;
        }

        betaTwoTimesSumOfX = 0;
        helper = xValues.head;
        for(int i = 0; i < this.dataLength; i++){
            betaTwoTimesSumOfX+=(helper.data);
            helper = helper.next;
        }

        betaThreeTimesSumOfY = 0;
        helper = yValues.head;
        for(int i = 0; i < this.dataLength; i++){
            betaThreeTimesSumOfY+=(helper.data);
            helper = helper.next;
        }

        double[] result = {betaZeroTimesN, betaOneTimesSumOfW, betaTwoTimesSumOfX, betaThreeTimesSumOfY};

        return result;   
    }

    double[] buildSecondRow(){
        double betaZeroTimesSumOfW;         //sum of w
        double betaOneTimesSumOfWSquared;   //sum of w^2
        double betaTwoTimesSumOfWTimesX;    //sum of wx
        double betaThreeTimesSumOfWTimesY;  //sum of wy

        Node leftHelper;
        Node rightHelper;

        betaZeroTimesSumOfW = 0;
        leftHelper = wValues.head;
        for(int i = 0; i < this.dataLength; i++){
            betaZeroTimesSumOfW+=leftHelper.data;
            leftHelper = leftHelper.next;
        }

        betaOneTimesSumOfWSquared = 0;
        leftHelper = wValues.head;
        for(int i = 0; i < this.dataLength; i++){
            betaOneTimesSumOfWSquared+=Math.pow(leftHelper.data,2);
            leftHelper = leftHelper.next;
        }

        betaTwoTimesSumOfWTimesX = 0;
        leftHelper = wValues.head;
        rightHelper = xValues.head;
        for(int i = 0; i < this.dataLength; i++){
            betaTwoTimesSumOfWTimesX+=(leftHelper.data*rightHelper.data);
            leftHelper = leftHelper.next;
            rightHelper = rightHelper.next;
        }

        betaThreeTimesSumOfWTimesY = 0;
        leftHelper = wValues.head;
        rightHelper = yValues.head;
        for(int i = 0; i < this.dataLength; i++){
            betaThreeTimesSumOfWTimesY+=(leftHelper.data*rightHelper.data);
            leftHelper = leftHelper.next;
            rightHelper = rightHelper.next;
        }

        double[] result = {betaZeroTimesSumOfW, betaOneTimesSumOfWSquared, betaTwoTimesSumOfWTimesX, betaThreeTimesSumOfWTimesY};
        return result;   
    }

    double[] buildThirdRow(){
        double betaZeroTimesSumOfX;       //x
        double betaOneTimesSumOfWTimesX;  //wx
        double betaTwoTimesXSquared;      //x^2
        double betaThreeTimesSumOfXTimesY;//xy

        Node leftHelper;
        Node rightHelper;

        betaZeroTimesSumOfX = 0;
        leftHelper = xValues.head;
        for(int i = 0; i < this.dataLength; i++){
            betaZeroTimesSumOfX += leftHelper.data;
            leftHelper = leftHelper.next;
        }

        betaOneTimesSumOfWTimesX = 0;
        leftHelper = wValues.head;
        rightHelper = xValues.head;
        for(int i = 0; i < this.dataLength; i++){
            betaOneTimesSumOfWTimesX+=(leftHelper.data*rightHelper.data);
            leftHelper = leftHelper.next;
            rightHelper = rightHelper.next;
        }

        betaTwoTimesXSquared = 0;
        leftHelper = xValues.head;
        for(int i = 0; i < this.dataLength; i++){
            betaTwoTimesXSquared+=Math.pow(leftHelper.data,2);
            leftHelper = leftHelper.next;
        }

        betaThreeTimesSumOfXTimesY  = 0;
        leftHelper = xValues.head;
        rightHelper = yValues.head;
        for(int i = 0; i < this.dataLength; i++){
            betaThreeTimesSumOfXTimesY += (leftHelper.data*rightHelper.data);
            leftHelper = leftHelper.next;
            rightHelper = rightHelper.next;
        }

        double [] result = {betaZeroTimesSumOfX, betaOneTimesSumOfWTimesX, betaTwoTimesXSquared, betaThreeTimesSumOfXTimesY};
        return result;   
    }

    double[] buildFourthRow(){
        double betaZeroTimesSumOfY;
        double betaOneTimesSumOfWTimesY;
        double betaTwoTimesSumOfXTimesY;
        double betaThreeTimesSumOfYSquared;

        Node leftHelper;
        Node rightHelper;

        betaZeroTimesSumOfY = 0;
        leftHelper = yValues.head;
        for(int i = 0; i < this.dataLength; i++){
            betaZeroTimesSumOfY += (leftHelper.data);
            leftHelper = leftHelper.next;
        }

        betaOneTimesSumOfWTimesY = 0;
        leftHelper = wValues.head;
        rightHelper = yValues.head;
        for(int i = 0; i < this.dataLength; i++){
            betaOneTimesSumOfWTimesY += (leftHelper.data*rightHelper.data);
            leftHelper = leftHelper.next;
            rightHelper = rightHelper.next;
        }

        betaTwoTimesSumOfXTimesY = 0;
        leftHelper = xValues.head;
        rightHelper = yValues.head;
        for(int i = 0; i < this.dataLength; i++){
            betaTwoTimesSumOfXTimesY += (leftHelper.data*rightHelper.data);
            leftHelper = leftHelper.next;
            rightHelper = rightHelper.next;
        }

        betaThreeTimesSumOfYSquared = 0;
        leftHelper = yValues.head;
        for(int i = 0; i < this.dataLength; i++){
            betaThreeTimesSumOfYSquared += Math.pow(leftHelper.data,2);
            leftHelper = leftHelper.next;
        }

        double[] result = {betaZeroTimesSumOfY, betaOneTimesSumOfWTimesY, betaTwoTimesSumOfXTimesY,
        betaThreeTimesSumOfYSquared};
        return result;   
    }

    double[] buildResultArr(){
        double sumOfZ = 0;       //z
        double sumOfWTimesZ = 0; //wz
        double sumOfXTimesZ = 0; //xz
        double sumOfYTimesZ = 0; //yz

        Node helper;
        Node zHelper;

        //only z -> sumOfZ
        zHelper = zValues.head;
        for(int i = 0; i < this.dataLength; i++){
            sumOfZ+= zHelper.data;
            zHelper = zHelper.next;
        }

        //only w and z -> sumOfWTimesZ
        helper = wValues.head;
        zHelper = zValues.head;
        for(int i = 0; i < this.dataLength; i++){
            sumOfWTimesZ+= (helper.data*zHelper.data);
            zHelper = zHelper.next;
            helper = helper.next;
        }

        //only x and z -> sumOfXTimesZ
        helper = xValues.head;
        zHelper = zValues.head;
        for(int i = 0; i < this.dataLength; i++){
            sumOfXTimesZ+= (helper.data*zHelper.data);
            zHelper = zHelper.next;
            helper = helper.next;
        }

        //onlye y and z -> sumOfYTimesZ
        helper = yValues.head;
        zHelper = zValues.head;
        for(int i = 0; i < this.dataLength; i++){
            sumOfYTimesZ+= (helper.data*zHelper.data);
            zHelper = zHelper.next;
            helper = helper.next;
        }

        double[] result = {sumOfZ, sumOfWTimesZ, sumOfXTimesZ, sumOfYTimesZ};
        return result;
    }

    void printDataQuartets(){
        Node wVal = this.wValues.head;
        Node xVal = this.xValues.head;
        Node yVal = this.yValues.head;
        Node zVal = this.zValues.head;
        
        System.out.printf("Data pairs:\n");

        while(xVal != null && yVal != null){
            System.out.printf("%.02f , %.02f , %.02f , %.02f\n",wVal.data,xVal.data,yVal.data,zVal.data);
            wVal = wVal.next;
            xVal = xVal.next;
            yVal = yVal.next;
            zVal = zVal.next;
        }
    }

    void printData(){
        printDataQuartets();
        System.out.printf("wSubK : %.02f. xSubK: %.02f. ySubK: %.02f. zSubK: %02f.\n",this.wSubK, this.xSubK, this.ySubK,this.zSubK);
        System.out.printf("betaZero:  %f. betaOne:  %f. betaTwo:  %f. betaThree: %f\n",betaZero, betaOne, betaTwo, betaThree);
        System.out.printf("Range: %f. UPI: %f. LPI: %f\n",this.range,this.upi,this.lpi);
    }

}