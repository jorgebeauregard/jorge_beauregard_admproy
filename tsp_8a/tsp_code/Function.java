abstract class Function{
    double x;
    double y;

    Function(double x){
        this.x = x;
        y = 0;
    }

    abstract double function(double x);

}