public class Main{
    static Reader reader;
    static List data;
    static MultipleRegression mr;
    public static void main(String []args){
        reader = new Reader();
        data = reader.readData();
        mr = new MultipleRegression(data);
        mr.printData();
    }
}