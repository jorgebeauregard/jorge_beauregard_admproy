public class Gauss {
    public static double mat[][];
    public static double results[];
    public static double coefficients[];

    Gauss(double mat[][], double results[]){
        this.mat = mat;
        this.results = results;
        this.coefficients = gaussSolver(this.mat,this.results);

    }

    double[] gaussSolver(double[][] mat, double[] results) {
        int n = results.length;

        for (int p = 0; p < n; p++) {
            int max = p;
            for (int i = p + 1; i < n; i++) {
                if (Math.abs(mat[i][p]) > Math.abs(mat[max][p])) {
                    max = i;
                }
            }
            //Swap row
            double[] temp = mat[p]; 
            mat[p] = mat[max]; 
            mat[max] = temp;

            //swap only value
            double t = results[p]; 
            results[p] = results[max]; 
            results[max] = t;

            for (int i = p + 1; i < n; i++) {
                double alpha = mat[i][p] / mat[p][p];
                results[i] -= alpha * results[p];
                for (int j = p; j < n; j++) {
                    mat[i][j] -= alpha * mat[p][j];
                }
            }
        }

        double[] x = new double[n];
        for (int i = n - 1; i >= 0; i--) {
            double sum = 0.0;
            for (int j = i + 1; j < n; j++) {
                sum += mat[i][j] * x[j];
            }
            x[i] = (results[i] - sum) / mat[i][i];
        }
        return x;
    }

}
