class Data{
	List lista;
	List listaX;
	List listaY;

	Data(List lista){
		this.lista = lista;
	}

		public double calcularSumatoria(int potencia){ //Sumatoria de Xn
			double acumulador=0;
			double sumatoria=0;
			sumatoria=0;
			Node n = new Node();
			n = lista.head;
			if(n==null){
				return(-1);
			}
			while(n!=null){
				acumulador+=Math.pow(n.data, potencia);
				n=n.next;
			}
			sumatoria=acumulador;
			acumulador=0;
			return(sumatoria);
		}

		public double calcularMedia(){ //Calcula la media de la sumatoria de la lista
			return(calcularSumatoria(1)/lista.obtenerNodos());
		}


		public double calcularSumatoria_X_Y(int potencia){ //Sumatoria de Xn+Yn en toda la lista ligada
			double acumulador=0;
			double sumatoria=0;
			List listaX = lista.separaX();
			List listaY = lista.separaY();
			Node n = new Node();
			Node m = new Node();
			n = listaX.head;
			m = listaY.head;
			while(n!=null){
				acumulador+=Math.pow(n.data*m.data, potencia);
				n=n.next;
				m=m.next;
			}
			sumatoria=acumulador;
			acumulador=0;
			return(sumatoria);
		}

		public void regresionLineal(){ //Calcula e imprime la regresion lineal de una lista con valores en X y otra lista con valores en Y
			List listaX = lista.separaX();
			List listaY = lista.separaY();
			Data x = new Data(listaX);
			Data y = new Data(listaY);
			Data z = new Data(lista);
			double n = listaX.obtenerNodos();
			double sum_media_x = x.calcularMedia();
			double sum_media_y = y.calcularMedia();
			double sum_x_y = z.calcularSumatoria_X_Y(1);
			double sum_x = x.calcularSumatoria(1);
			double sum_y = y.calcularSumatoria(1);
			double sum_x_cuadrada = x.calcularSumatoria(2);
			double sum_y_cuadrada = y.calcularSumatoria(2);

			double b1 = (sum_x_y-(n*sum_media_x*sum_media_y))/(sum_x_cuadrada-(n*Math.pow(sum_media_x,2)));
			double b0 = sum_media_y-(b1*sum_media_x);
			double r_xy = ((n * sum_x_y) - (sum_x*sum_y)) / (Math.sqrt( (n*sum_x_cuadrada-Math.pow(sum_x,2)) * (n*sum_y_cuadrada-Math.pow(sum_y,2)) ));
			double r_squared = Math.pow(r_xy,2);
			double y_k = b0+b1*386;

			System.out.println();
			System.out.println("Elementos en X");
			listaX.imprimirLista();
			System.out.println("Elementos en Y");
			listaY.imprimirLista();
			System.out.println();
			System.out.println("b1 =  " + b1);
			System.out.println("b0 = " + b0);
			System.out.println("r_xy = " + r_xy);
			System.out.println("r_cuadrada = " + r_squared);
			System.out.println("y_k = " + y_k);
		}
}