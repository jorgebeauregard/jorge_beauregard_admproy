import java.util.Scanner;

public class Lector{
	Scanner sc;

	Lector(){
		sc = new Scanner(System.in);
	}

	public List leerDatos(){ //Lee los datos de un input y los mete en una lista ligada
		List miLista = new List();
		while(sc.hasNext()){
			double data = sc.nextDouble();
			Node newNode = new Node(data);
			miLista.insertarElemento(newNode);
		}
		sc=null;
		return miLista;
	}
}