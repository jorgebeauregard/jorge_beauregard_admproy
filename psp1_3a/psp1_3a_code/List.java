public class List{
	Node head;

	public boolean insertarElemento(Node newNode){ //Insertar cualquier elemento en la lista
		Node head = new Node();
		if(this.head==null){
			this.head=newNode;
		}
		else{
			head = this.head;
			while(head.next!=null){
				head=head.next;
			}
			head.next=newNode;
			head=head.next;
		}
		return true;
	}

	public void imprimirLista(){ //Imprime cualquier lista ligada
		if(this.head==null) return;
		Node head = new Node();
		head=this.head;
		while(head!=null){
			System.out.print(head.data + " -> ");
			head=head.next;
		}
		System.out.println();
	}

	public int obtenerNodos(){ //Obtener la cantidad de nodos que tiene cierta lista
		int nodos=0;
		Node head = this.head;
		while(head!=null){
			head=head.next;
			nodos++;
		}
		return(nodos);
	}

	public List separaX(){ //Asumiendo que nuestro input cuenta con valores en X y en Y, mete únicamente a los valores en X a una lista ligada diferente
		int contador = 1;
		Node data;
		List listaX = new List();
		Node head = new Node();
		head = this.head;
		while(head!=null){
			if(contador%2!=0){
				Node current = new Node(head.data);
				listaX.insertarElemento(current);		
			}
			head=head.next;
			contador++;
		}		
		return listaX;
	}

	public List separaY(){ //Lo mismo que separaX, pero para los datos en Y
		int contador = 1;
		Node data;
		List listaY = new List();
		Node head = new Node();
		head = this.head;
		while(head!=null){
			if(contador%2==0){
				Node current = new Node(head.data);
				listaY.insertarElemento(current);		
			}
			head=head.next;
			contador++;
		}		
		return listaY;
	}

}
